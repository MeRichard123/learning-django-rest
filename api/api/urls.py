from django.urls import path,include
from .views import ArticleDetailClass, ListArticlesClass, ArticleViewSet
from .views import listArticlesFunc, articleDetailFunc, GenericApiView
from rest_framework.routers import DefaultRouter

# For viewSets
router = DefaultRouter()
router.register("",ArticleViewSet, basename="article")

urlpatterns = [
    # Class Views
    path("articles-class/",ListArticlesClass.as_view()),
    path("article-class/<int:id>", ArticleDetailClass.as_view()),
    
    # Function Views 
    path("articles-func/",listArticlesFunc),
    path("article-func/<int:id>/", articleDetailFunc),

    # Generic Views
    path("articles-generic/",GenericApiView.as_view()),
    path("articles-generic/<int:id>/",GenericApiView.as_view()),

    # ViewSets
    path("viewset/",include(router.urls)),
    path("viewset/<int:pk>/",include(router.urls))

]