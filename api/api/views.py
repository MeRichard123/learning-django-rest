#from django.http import HttpResponse, JsonResponse
from .models import Article
from .serializers import ArticleSerializer
from django.shortcuts import get_object_or_404
from rest_framework.decorators import api_view
from rest_framework.response import Response
from rest_framework import status
from rest_framework.views import APIView
from rest_framework import generics
from rest_framework import mixins
from rest_framework import viewsets
# Authentication
#from rest_framework.authentication import SessionAuthentication,BasicAuthentication
from rest_framework.authentication import TokenAuthentication
from rest_framework.permissions import IsAuthenticated

ExtendClasses = [mixins.ListModelMixin, mixins.CreateModelMixin, mixins.UpdateModelMixin, mixins.RetrieveModelMixin, mixins.DestroyModelMixin]

# Create your views here.
#? Generic class view 
class GenericApiView(generics.GenericAPIView, *ExtendClasses,):
    serializer_class = ArticleSerializer
    queryset = Article.objects.all()
    #authentication_classes = [SessionAuthentication, BasicAuthentication]
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsAuthenticated]

    lookup_field = "id"

    def get(self, req, id=None):
        if id:
            return self.retrieve(req)
        else:    
            return self.list(req)

    def post(self, req):
        return self.create(req)

    def put(self,req,id=None):
        return self.update(req,id)

    def delete(self, req, id):
        return self.destroy(req,id)


# To use a ViewSet you need to bind the methods to the urls with a router
class ArticleViewSet(viewsets.GenericViewSet, *ExtendClasses):
    permission_classes = [IsAuthenticated]
    serializer_class = ArticleSerializer
    queryset = Article.objects.all()

#? Class Based View
class ListArticlesClass(APIView):
    def get(self, req):
        # get all articles 
        articles = Article.objects.all()
        # Serialize into JSON
        serializer = ArticleSerializer(articles, many=True)
        return Response(serializer.data)
    
    def post(self, req):
        serializer = ArticleSerializer(data=req.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors,status=status.HTTP_400_BAD_REQUEST) 




#? Class Based View
class ArticleDetailClass(APIView):
    def get_object(self, id):
        try:
            return Article.objects.get(id=id)
        except Article.DoesNotExist:
            return Response("Article Not found", status=status.HTTP_404_NOT_FOUND)
            
    def get(self, req, id):
        article = self.get_object(id)
        serializer = ArticleSerializer(article)
        return Response(serializer.data)

    def put(self, req, id):
        article = self.get_object(id)
        serializer = ArticleSerializer(article, data=req.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def delete(self, req, id):
        article = self.get_object(id)
        article.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)

#? Function Based View 
@api_view(['GET','POST'])
def listArticlesFunc(req):
    # many=True serializer a querySet
    if req.method == "GET":
        articles = Article.objects.all() 
        serializer = ArticleSerializer(articles, many=True)
        return Response(serializer.data)

    elif req.method == "POST":
        serializer = ArticleSerializer(data=req.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors,status=status.HTTP_400_BAD_REQUEST)                   
        
    
#? Function View
@api_view(['GET','PUT','DELETE'])
def articleDetailFunc(req, pk):
    try:
        article = Article.objects.get(pk=pk)
    except Article.DoesNotExist:
        return Response("Article Not found",status=status.HTTP_404_NOT_FOUND)

    if req.method == "GET":
        serializer = ArticleSerializer(article)
        return Response(serializer.data)
    
    elif req.method == "PUT":
        serializer = ArticleSerializer(article, data=req.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
    
    elif req.method == "DELETE":
        article.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)

       
