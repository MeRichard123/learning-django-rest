# Fullstack React and Django API blog app

## API Routes
```
1.     admin/
2.     articles-class/
3.     article-class/<int:id>
4.     articles-func/
5.     article-func/<int:id>/
6.     articles-generic/
7.     articles-generic/<int:id>/
8.     viewset/
9.     viewset/<int:pk>/
10.    api/auth
11.    api/auth/register
12.    api/auth/login
13.    api/auth/user
14.    api/auth/logout [name='knox-logout']
```
Each route demonstrates a different type of Django Rest Framework view 
- A class based crud view (APIView)
- A function based crud view
- A generic crud view (GenericAPIView) (With Permissions)
- A generic viewset crud view (With Permissions)
- Knox auth routes for authentication 

The frontend uses Zustand for state management and Axios for API calls
