import create from "zustand";
import { persist } from "zustand/middleware";

const useAuth = create(
  persist(
    (set) => ({
      token: 0,
      isAuthenticated: false,
      setToken: (auth_token) => set((state) => ({ token: auth_token })),
      setIsAuthenticated: (bool) => set((state) => ({ isAuthenticated: bool })),
    }),
    {
      name: "state",
    }
  )
);

export default useAuth;
