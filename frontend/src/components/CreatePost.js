import React, { useState } from "react";
import { makeStyles } from "@material-ui/core/styles";
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";
import axios from "axios";

const useStyles = makeStyles((theme) => ({
  root: {
    display: "flex",
    flexDirection: "column",
    "& > *": {
      margin: theme.spacing(1),
      width: "25ch",
    },
  },
  container: {
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
  },
}));

const CreatePost = () => {
  const [title, setTitle] = useState("");
  const [author, setAuthor] = useState("");
  const classes = useStyles();
  const token = JSON.parse(localStorage.getItem("state")).token;

  const handleSubmit = async (e) => {
    e.preventDefault();
    await axios.post(
      "http://localhost:8000/viewset/",
      {
        title: title,
        author: author,
      },
      {
        headers: {
          Authorization: `token ${token}`,
        },
      }
    );
    setTitle("");
    setAuthor("");
    window.location.reload();
  };

  return (
    <div className={classes.container}>
      <form onSubmit={handleSubmit} className={classes.root}>
        <TextField
          id="standard-basic"
          label="Title"
          onChange={(e) => setTitle(e.target.value)}
        />
        <TextField
          id="standard-basic"
          label="Author"
          onChange={(e) => setAuthor(e.target.value)}
        />
        <Button type="submit" variant="contained">
          Create New Post
        </Button>
      </form>
    </div>
  );
};

export default CreatePost;
