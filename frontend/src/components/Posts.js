import React, { useEffect, useState } from "react";
import axios from "axios";
import { makeStyles } from "@material-ui/core/styles";
import Paper from "@material-ui/core/Paper";
import { Link } from "react-router-dom";

const useStyles = makeStyles((theme) => ({
  root: {
    display: "flex",
    flexWrap: "wrap",
    justifyContent: "center",
    "& > *": {
      margin: theme.spacing(1),
      width: theme.spacing(16),
      height: theme.spacing(16),
    },
  },
  title: {
    margin: "10px;",
    textAlign: "center",
  },
  card: {
    width: "10rem",
    height: "10rem",
    padding: "15px",
    display: "flex",
    alignItems: "center",
    flexDirection: "column",
    justifyContent: "center",
    transition: "transform 250ms ease",
    "&:hover": {
      transform: "scale(1.1)",
    },
  },
  link: {
    textDecoration: "none",
    color: "#000",
  },
}));

const Posts = () => {
  const classes = useStyles();
  const [posts, setPosts] = useState([]);
  const [isLoading, setLoading] = useState(true);
  const token = JSON.parse(localStorage.getItem("state")).token;

  useEffect(() => {
    const getPosts = async () => {
      const { data } = await axios.get("http://localhost:8000/viewset/", {
        headers: {
          Authorization: `token ${token}`,
        },
      });
      setPosts(data);
      setLoading(false);
    };
    getPosts();
  }, []);
  return (
    <div className={classes.root}>
      {isLoading && <h1>Loading</h1>}
      {posts.map((post) => (
        <Paper elevation={3} key={post.id} className={classes.card}>
          <Link to={`/post/${post.id}`} className={classes.link}>
            <h2 className={classes.title}>{post.title}</h2>
          </Link>
          <small>{post.author}</small>
        </Paper>
      ))}
    </div>
  );
};

export default Posts;
