import React from "react";
import { Route, Redirect } from "react-router-dom";

const PrivateRoute = ({ component: Component, ...rest }) => {
  const state = JSON.parse(localStorage.getItem("state"));
  return (
    <Route
      {...rest}
      render={(routeProps) =>
        state !== null ? (
          <Component {...routeProps} />
        ) : (
          <Redirect to="/login" />
        )
      }
    />
  );
};
export default PrivateRoute;
