import React, { useEffect, useState } from "react";
import axios from "axios";
import { useParams } from "react-router-dom";
import { Link } from "react-router-dom";
import Button from "@material-ui/core/Button";
import { makeStyles } from "@material-ui/core/styles";
import TextField from "@material-ui/core/TextField";

const useStyles = makeStyles((theme) => ({
  root: {
    display: "flex",
    flexDirection: "column",
    "& > *": {
      margin: theme.spacing(1),
      width: "25ch",
    },
  },
  container: {
    height: "80vh",
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
  },
}));

const PostDetail = () => {
  const token = JSON.parse(localStorage.getItem("state")).token;
  const [title, setTitle] = useState("");
  const [isLoading, setLoading] = useState(true);
  const [author, setAuthor] = useState("");
  const [newTitle, setNewTitle] = useState("");
  const [newAuthor, setNewAuthor] = useState("");
  const [date, setDate] = useState("");
  const { id } = useParams();
  useEffect(() => {
    const getPostDetail = async () => {
      const res = await axios.get(`http://localhost:8000/viewset/${id}`, {
        headers: {
          Authorization: `token ${token}`,
        },
      });
      setTitle(res.data.title);
      setAuthor(res.data.author);
      setDate(res.data.date);
      setLoading(false);
      setNewAuthor(res.data.author);
      setNewTitle(res.data.title);
    };
    getPostDetail();
  }, []);
  let dateString = new Date(date);
  dateString = dateString.toLocaleDateString();

  const removePost = async () => {
    await axios.delete(`http://localhost:8000/viewset/${id}`, {
      headers: {
        Authorization: `token ${token}`,
      },
    });
    window.location = "/";
  };

  const handleSubmit = async (e) => {
    e.preventDefault();
    await axios.put(
      `http://localhost:8000/viewset/${id}/`,
      {
        title: newTitle,
        author: newAuthor,
      },
      {
        headers: {
          Authorization: `token ${token}`,
        },
      }
    );
    setTitle("");
    setAuthor("");
    window.location.reload();
  };

  const classes = useStyles();

  return (
    <div>
      {isLoading && <h1>Loading</h1>}

      <Link to="/">
        <Button href="#" color="primary">
          Back
        </Button>
      </Link>
      <h1>{title}</h1>
      <h2>{author}</h2>
      <h3>{dateString}</h3>
      <Button variant="contained" color="secondary" onClick={removePost}>
        Delete
      </Button>
      <div className={classes.container}>
        <form onSubmit={handleSubmit} className={classes.root}>
          <TextField
            id="standard-basic"
            value={newTitle}
            onChange={(e) => setNewTitle(e.target.value)}
          />
          <TextField
            id="standard-basic"
            value={newAuthor}
            onChange={(e) => setNewAuthor(e.target.value)}
          />
          <Button type="submit" variant="contained">
            Update Post
          </Button>
        </form>
      </div>
    </div>
  );
};

export default PostDetail;
