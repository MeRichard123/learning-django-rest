import React, { useEffect } from "react";
import axios from "axios";
import useAuth from "../store";

const Logout = () => {
  const token = JSON.parse(localStorage.getItem("state"));

  const setToken = useAuth((state) => state.setToken);
  const setIsAuthenticated = useAuth((state) => state.setIsAuthenticated);
  console.log(token);
  useEffect(() => {
    const logout = async () => {
      await axios.post("http://localhost:8000/api/auth/logout", null, {
        headers: {
          Authorization: `Token ${token.token}`,
        },
      });
      await setToken(0);
      await setIsAuthenticated(false);
      await localStorage.removeItem("state");
    };
    logout();
  }, []);
  return <div>You have been logged out</div>;
};

export default Logout;
