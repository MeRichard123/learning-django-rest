import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import Typography from "@material-ui/core/Typography";
import Button from "@material-ui/core/Button";
import { Link } from "react-router-dom";
import useAuth from "../store";

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
  },
  title: {
    flexGrow: 1,
  },
  link: {
    color: "#fff",
    textDecoration: "none",
  },
}));

const Header = () => {
  const classes = useStyles();
  const IsAuthenticated = useAuth((state) => state.isAuthenticated);
  console.log(IsAuthenticated);

  return (
    <AppBar position="static">
      <Toolbar>
        <Typography variant="h6" className={classes.title}>
          Posts
        </Typography>
        <nav>
          {IsAuthenticated ? (
            <Button color="inherit">
              <Link to="/logout" className={classes.link}>
                Logout
              </Link>
            </Button>
          ) : (
            <>
              <Button color="inherit">
                <Link to="/login" className={classes.link}>
                  Login
                </Link>
              </Button>
              <Button color="inherit">
                <Link to="/register" className={classes.link}>
                  Register
                </Link>
              </Button>
            </>
          )}
        </nav>
      </Toolbar>
    </AppBar>
  );
};

export default Header;
