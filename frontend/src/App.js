import "./App.css";
import CreatePost from "./components/CreatePost";
import Header from "./components/Header";
import Posts from "./components/Posts";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import Login from "./components/Login";
import Register from "./components/Register";
import Logout from "./components/Logout";
import PrivateRoute from "./components/PrivateRoute";
import PostDetail from "./components/PostDetail";

function App() {
  return (
    <Router>
      <div className="App">
        <Header />
        <Switch>
          <PrivateRoute path="/" exact component={HomePage} />
          <Route path="/post/:id" component={PostDetail} />
          <Route path="/login" component={Login} />
          <Route path="/register" component={Register} />
          <Route path="/logout" component={Logout} />
        </Switch>
      </div>
    </Router>
  );
}

export default App;

const HomePage = () => {
  return (
    <>
      <Posts />
      <CreatePost />
    </>
  );
};
